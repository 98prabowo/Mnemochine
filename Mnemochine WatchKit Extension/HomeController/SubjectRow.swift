//
//  RowController.swift
//  Mnemochine WatchKit Extension
//
//  Created by Dimas A. Prabowo on 24/08/21.
//

import UIKit
import WatchKit

class SubjectRow: NSObject {
    @IBOutlet weak var groupRow: WKInterfaceGroup!
    @IBOutlet weak var myLabel: WKInterfaceLabel!
}
