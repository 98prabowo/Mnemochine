//
//  SubjectController.swift
//  Mnemochine WatchKit Extension
//
//  Created by Dimas A. Prabowo on 23/08/21.
//

import WatchKit
import Foundation
import WatchConnectivity

class SubjectController: WKInterfaceController {
    @IBOutlet weak var interfaceTable: WKInterfaceTable!
    
    private var subjects = [SubjectData]()
    private var test = [String]()
    
    var watchSession: WCSession?
    
    override init() {
        super.init()
        
        if WCSession.isSupported() {
            watchSession = WCSession.default
            watchSession?.delegate = self
            watchSession?.activate()
        }
    }
    
    override func awake(withContext context: Any?) {
        self.setTitle("Topics")
        configureTable()
    }
    
    func configureTable() {
        self.interfaceTable.setNumberOfRows(subjects.count, withRowType: "cell")
        for index in 0..<self.interfaceTable.numberOfRows {
            if let row = interfaceTable.rowController(at: index) as? SubjectRow {
                row.myLabel.setText(subjects[index].name)
                if let color = subjects[index].materials.first?.color {
                    row.groupRow.setBackgroundColor(UIColor(named: color))
                }
            }
        }
    }
    
    override func contextForSegue(withIdentifier segueIdentifier: String, in table: WKInterfaceTable, rowIndex: Int) -> Any? {
        var data = [MaterialData]()
        if segueIdentifier == "detailSegue" {
            data =  subjects[rowIndex].materials
        }
        return data
    }
    
    override func willActivate() {
        print("Activate")
    }
    
    override func didDeactivate() {
        print("Deactivated")
    }

}

extension SubjectController: WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        print("Session activation did complete")
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("didReceiveMessage")
        if let data = message["subjects"] as? Data {
            do {
                let decodedData = try JSONDecoder().decode(SubjectData.self, from: data)
                self.subjects.append(decodedData)
            } catch {
                print("Error decode: \(error.localizedDescription)")
            }
        }
        
        configureTable()
    }
    
    func session(_ session: WCSession, didReceiveApplicationContext applicationContext: [String : Any]) {
        print("didReceiveApplicationContext")
        if let data = applicationContext["subjects"] as? Data {
            do {
                let decodedData = try JSONDecoder().decode(SubjectData.self, from: data)
                self.subjects.append(decodedData)
            } catch {
                print("Error decode: \(error.localizedDescription)")
            }

        }
        configureTable()
    }
}
