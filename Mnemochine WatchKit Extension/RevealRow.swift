//
//  RevealRow.swift
//  Mnemochine WatchKit Extension
//
//  Created by Dimas A. Prabowo on 24/08/21.
//

import WatchKit

class RevealRow: NSObject {
    @IBOutlet weak var revealButton: WKInterfaceButton!
    var qnaController: QNAController!
    var material: MaterialData?
    var indexRow: Int?

    @IBAction func revealTapped() {
        if let material = self.material {
            qnaController.configureTableAnswer(with: material)
        }
    }
    
    func configure(with material: MaterialData, indexRow: Int) {
        self.material = material
        self.indexRow = indexRow
    }
}
