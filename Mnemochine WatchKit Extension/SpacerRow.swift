//
//  SpacerRow.swift
//  Mnemochine WatchKit Extension
//
//  Created by Dimas A. Prabowo on 25/08/21.
//

import UIKit
import  WatchKit

class SpacerRow: NSObject {
    @IBOutlet weak var group: WKInterfaceGroup!
    @IBOutlet weak var spacerButton: WKInterfaceButton!
}
