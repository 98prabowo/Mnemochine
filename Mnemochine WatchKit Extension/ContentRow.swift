//
//  ContentRow.swift
//  Mnemochine WatchKit Extension
//
//  Created by Dimas A. Prabowo on 24/08/21.
//

import UIKit
import WatchKit

class ContentRow: NSObject {
    @IBOutlet weak var content: WKInterfaceLabel!
}
