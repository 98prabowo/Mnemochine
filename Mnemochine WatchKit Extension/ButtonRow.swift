//
//  ButtonRow.swift
//  Mnemochine WatchKit Extension
//
//  Created by Dimas A. Prabowo on 24/08/21.
//

import WatchKit

class ButtonRow: NSObject {
    @IBOutlet weak var easyButton: WKInterfaceButton!
    @IBOutlet weak var normalButton: WKInterfaceButton!
    @IBOutlet weak var hardButton: WKInterfaceButton!
    @IBOutlet weak var skipButton: WKInterfaceButton!
    
    var qnaController: QNAController!
    var material: MaterialData?
    var indexRow: Int?
    
    @IBAction func easyTapped() {
        discardMaterial()
        if let testMaterial = qnaController.getMaterial() {
            qnaController.configureTableQuestion(with: testMaterial)
        }
    }
    
    @IBAction func normalTapped() {
        if let material = self.material {
            discardMaterial()
            qnaController.normalMaterials.append(material)
            if let testMaterial = qnaController.getMaterial() {
                qnaController.configureTableQuestion(with: testMaterial)
            }
        }
    }
    
    @IBAction func hardTapped() {
        if let material = self.material {
            discardMaterial()
            qnaController.hardMaterials.append(material)
            if let testMaterial = qnaController.getMaterial() {
                qnaController.configureTableQuestion(with: testMaterial)
            }
        }
    }
    
    @IBAction func skipTapped() {
        if let material = self.material {
            discardMaterial()
            qnaController.normalMaterials.append(material)
            if let testMaterial = qnaController.getMaterial() {
                qnaController.configureTableQuestion(with: testMaterial)
            }
        }
    }
    
    /// Discard or move `MaterialData` object from or to specific array based on button that user tapped.
    func discardMaterial() {
        for material in qnaController.materials {
            if material === self.material {
                qnaController.materials.removeFirst()
            }
        }
        
        for material in qnaController.normalMaterials {
            if material === self.material {
                qnaController.normalMaterials.removeFirst()
            }
        }
        
        for material in qnaController.hardMaterials {
            if material === self.material {
                qnaController.hardMaterials.removeFirst()
            }
        }
    }
    
    func configure(with material: MaterialData, indexRow: Int) {
        self.material = material
        self.indexRow = indexRow
    }
}
