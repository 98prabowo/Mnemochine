//
//  QNAController.swift
//  Mnemochine WatchKit Extension
//
//  Created by Dimas A. Prabowo on 24/08/21.
//

import UIKit
import WatchKit

class QNAController: WKInterfaceController {
    @IBOutlet weak var interfaceTable: WKInterfaceTable!
    
    var materials = [MaterialData]()
    var hardMaterials = [MaterialData]()
    var normalMaterials = [MaterialData]()
    private var selectedRow: Int = 0
    private var isReveal: Bool = false
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        if let data = context as? [MaterialData] {
            self.materials = data.shuffled()
        }
        
        if let material = getMaterial() {
            configureTableQuestion(with: material)
        }
    }
    
    /// get `MaterialData` Object that will shown in session from materials, normal materials, and hard materials array.
    ///
    /// - Returns: Instance of optional `MaterialData` Object. If none `Material Data` provided, sessions will be ended.
    func getMaterial() -> MaterialData? {
        var result: MaterialData?
        if !self.materials.isEmpty {
            if let material = self.materials.first {
                result = material
            }
        } else if !self.hardMaterials.isEmpty {
            if let material = self.hardMaterials.first {
                result = material
            }
        } else if !self.normalMaterials.isEmpty {
            if let material = self.normalMaterials.first {
                result = material
            }
        } else {
            clearTable()
        }
        
        return result
    }
    
    // MARK: - Clear Table
    func clearTable() {
        self.interfaceTable.setNumberOfRows(1, withRowType: "finishCell")
        for i in 0..<self.interfaceTable.numberOfRows {
            if let _ = interfaceTable.rowController(at: i) as? SubjectRow {
            }
        }
    }
    
    // MARK: - Show Answer
    func configureTableAnswer(with material: MaterialData) {
        if let imageData = material.image,
           let imageString = material.imageString {
            self.interfaceTable.setRowTypes(["spacer", "imageCell", "contentCell", "contentCell", "headerCell", "buttonCell"])
            var color = ""
            if let _color = material.color {
                color = _color
            }
            
            for index in 0...5 {
                switch index {
                case 0:
                    if let row = interfaceTable.rowController(at: index) as? SpacerRow {
                        row.spacerButton.setBackgroundColor(UIColor.clear)
                        row.spacerButton.setTitle("Answer")
                    }
                case 1:
                    if let row = interfaceTable.rowController(at: index) as? ImageRow {
                        if material.isFromUser {
                            row.image.setImage(UIImage(data: imageData))
                        } else {
                            row.image.setImage(UIImage(systemName: imageString))
                            row.image.setTintColor(UIColor(named: color))
                        }
                    }
                case 2:
                    if let row = interfaceTable.rowController(at: index) as? ContentRow {
                        row.content.setText(material.question)
                        row.content.setTextColor(UIColor(named: color))
                    }
                case 3:
                    if let row = interfaceTable.rowController(at: index) as? ContentRow {
                        row.content.setText(material.answer)
                        row.content.setTextColor(UIColor(named: color))
                    }
                default:
                    if let row = interfaceTable.rowController(at: index) as? ButtonRow {
                        row.qnaController = self
                        row.configure(with: material, indexRow: index)
                    }
                }
            }
        } else {
            self.interfaceTable.setRowTypes(["spacer", "contentCell", "contentCell", "headerCell", "buttonCell"])
            var color = ""
            if let _color = material.color {
                color = _color
            }
            
            for index in 0...4 {
                switch index {
                case 0:
                    if let row = interfaceTable.rowController(at: index) as? SpacerRow {
                        row.spacerButton.setBackgroundColor(UIColor.clear)
                        row.spacerButton.setTitle("Answer")
                    }
                case 1:
                    if let row = interfaceTable.rowController(at: index) as? ContentRow {
                        row.content.setText(material.question)
                        row.content.setTextColor(UIColor(named: color))
                    }
                case 2:
                    if let row = interfaceTable.rowController(at: index) as? ContentRow {
                        row.content.setText(material.answer)
                        row.content.setTextColor(UIColor(named: color))
                    }
                default:
                    if let row = interfaceTable.rowController(at: index) as? ButtonRow {
                        row.qnaController = self
                        row.configure(with: material, indexRow: index)
                    }
                }
            }
        }
        
    }
    
    // MARK: - Show Question
    func configureTableQuestion(with material: MaterialData) {
        if let imageData = material.image,
           let imageString = material.imageString {
            self.interfaceTable.setRowTypes(["spacer", "imageCell", "contentCell", "revealCell"])
            var color = ""
            if let _color = material.color {
                color = _color
            }
            
            for index in 0...3 {
                switch index {
                case 0:
                    if let row = interfaceTable.rowController(at: index) as? SpacerRow {
                        row.spacerButton.setBackgroundColor(UIColor.clear)
                        row.spacerButton.setTitle("Question")
                    }
                case 1:
                    if let row = interfaceTable.rowController(at: index) as? ImageRow {
                        if material.isFromUser {
                            row.image.setImage(UIImage(data: imageData))
                        } else {
                            row.image.setImage(UIImage(systemName: imageString))
                            row.image.setTintColor(UIColor(named: color))
                        }
                    }
                case 2:
                    if let row = interfaceTable.rowController(at: index) as? ContentRow {
                        row.content.setText(material.question)
                        row.content.setTextColor(UIColor(named: color))
                    }
                default:
                    if let row = interfaceTable.rowController(at: index) as? RevealRow {
                        row.qnaController = self
                        row.configure(with: material, indexRow: index)
                    }
                }
            }
        } else {
            self.interfaceTable.setRowTypes(["spacer", "contentCell", "revealCell"])
            var color = ""
            if let _color = material.color {
                color = _color
            }
            
            for index in 0...2 {
                switch index {
                case 0:
                    if let row = interfaceTable.rowController(at: index) as? SpacerRow {
                        row.spacerButton.setBackgroundColor(UIColor.clear)
                        row.spacerButton.setTitle("Question")
                    }
                case 1:
                    if let row = interfaceTable.rowController(at: index) as? ContentRow {
                        row.content.setText(material.question)
                        row.content.setTextColor(UIColor(named: color))
                    }
                default:
                    if let row = interfaceTable.rowController(at: index) as? RevealRow {
                        self.isReveal = true
                        row.qnaController = self
                        row.configure(with: material, indexRow: index)
                    }
                }
            }
        }
    }
    
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        self.selectedRow = rowIndex
    }
    
    override func willActivate() {
        print("Activate")
    }
    
    override func didDeactivate() {
        print("Deactivated")
    }
}
