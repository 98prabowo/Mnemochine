//
//  NibExtension.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 06/09/21.
//

import Foundation
import UIKit

protocol NibGenerator {
    static var identifier: String { get }
    
    /// Create an instance on `UINib` Object.
    ///
    /// - Returns: Instance of `UINib` Object.
    static func nib() -> UINib
}

extension NibGenerator {
    static var identifier: String {
        String(describing: Self.self)
    }
    
    static func nib() -> UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
}
