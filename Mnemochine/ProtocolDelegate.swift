//
//  ProtocolDelegate.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 27/08/21.
//

import Foundation

protocol UpdateItem {
    /// Refresh specific UIViewController class data.
    func refreshItem()
}

protocol UpdateDetailMaterial {
    /// Update material data with new material from parameter.
    ///
    /// - Parameters:
    ///     - material: Instance of `MaterialData` Object that will send to target Class.
    func updateMaterial(with material: Material)
}
