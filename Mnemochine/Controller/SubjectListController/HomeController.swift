//
//  ViewController.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 23/08/21.
//

import UIKit

class HomeController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var subjects = [Subject]()
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Topics"
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.subjects = CoreDataManager.shared.fetchData()
        
        if self.subjects.isEmpty {
            PopulateData.shared.start()
            
            self.subjects = CoreDataManager.shared.fetchData()
        }
        
        let settingButton = UIBarButtonItem(image: UIImage(systemName: "gearshape"), style: .plain, target: self, action: #selector(settingTapped))
        let addButton = UIBarButtonItem(image: UIImage(systemName: "square.and.pencil"), style: .plain, target: self, action: #selector(addTapped))
        self.navigationItem.rightBarButtonItems = [settingButton, addButton]
        self.navigationController?.navigationBar.tintColor = .label
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.registerCell()
        
        createObserver()
    }
    
    private func registerCell() {
        self.collectionView.register(HomeCell.nib(), forCellWithReuseIdentifier: HomeCell.identifier)
    }
    
    @objc func settingTapped(_ sender: Any) {
        let settingController = SettingController(nibName: SettingController.identifier, bundle: nil)
        settingController.updateSubjects = self
        self.navigationController?.pushViewController(settingController, animated: true)
    }
    
    @objc func addTapped(_ sender: Any) {
        let newSubjectController = NewSubjectController(nibName: NewSubjectController.identifier, bundle: nil)
        newSubjectController.startSubjectController = self
        self.navigationController?.present(newSubjectController, animated: true, completion: nil)
    }
}

// MARK: - Collection View
extension HomeController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return subjects.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCell.identifier, for: indexPath) as? HomeCell else { return UICollectionViewCell() }
        if let materialCount = self.subjects[indexPath.row].materialChild?.count {
            cell.materialCount.text = "\(materialCount) Materials"
        } else {
            cell.materialCount.text = "No Materials"
        }
        
        if let materials = subjects[indexPath.row].materialChild?.allObjects as? [Material],
           let color = materials.first?.color {
            cell.contentView.backgroundColor = UIColor(named: color)
        }
        
        cell.header.text = self.subjects[indexPath.row].name
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.collectionView.deselectItem(at: indexPath, animated: true)
        
        let detailListController = DetailListController(nibName: DetailListController.identifier, bundle: nil)
        detailListController.deleteMaterial = self
        detailListController.configure(with: subjects[indexPath.row])
        self.navigationController?.pushViewController(detailListController, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 180, height: 180)
    }
}

// MARK: - Observer Notification
extension HomeController {
    func createObserver() {
        let name = Notification.Name(refreshHomeKey)
        NotificationCenter.default.addObserver(self, selector: #selector(addItem), name: name, object: nil)
    }
    
    @objc private func addItem(_ notification: NSNotification) {
        self.subjects = CoreDataManager.shared.fetchData()
        
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
}

// MARK: - Protocol Delegate
extension HomeController: UpdateItem {
    func refreshItem() {
        self.subjects = CoreDataManager.shared.fetchData()
        
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
}
