//
//  HomeCell.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 26/08/21.
//

import UIKit

class HomeCell: UICollectionViewCell, NibGenerator {
    @IBOutlet weak var materialCount: UILabel!
    @IBOutlet weak var header: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.layer.cornerRadius = 10
    }

}
