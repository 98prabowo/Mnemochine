//
//  DetailMaterialController.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 24/08/21.
//

import UIKit

class DetailMaterialController: UIViewController, NibGenerator {
    @IBOutlet weak var tableView: UITableView!
    
    private var subject: Subject?
    private var material: Material?
    var refreshMaterialList: UpdateItem!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Material Data"
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Material Data"
        self.navigationController?.navigationBar.prefersLargeTitles = false
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.registeTableCell()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(editTapped))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.prefersLargeTitles = true
        refreshMaterialList.refreshItem()
    }
    
    @objc func editTapped(_ sender: Any) {
        if let subject = self.subject,
           let material = self.material {
            let editMaterialController = EditMaterialController(nibName: EditMaterialController.identifier, bundle: nil)
            editMaterialController.detailMaterialController = self
            editMaterialController.configure(with: subject,and: material)
            self.present(editMaterialController, animated: true, completion: nil)
        }
    }
    
    private func registeTableCell() {
        self.tableView.register(ImageDetailCell.nib(), forCellReuseIdentifier: ImageDetailCell.identifier)
        self.tableView.register(ContentDetailCell.nib(), forCellReuseIdentifier: ContentDetailCell.identifier)
    }
    
    func configure(with subject: Subject, and material: Material) {
        self.subject = subject
        self.material = material
    }
}

// MARK: - Table View
extension DetailMaterialController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let material = material,
           let _ = material.image {
            return 3
        } else {
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let material = material,
           let image = material.image {
            switch indexPath.row {
            case 0:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: ImageDetailCell.identifier, for: indexPath) as? ImageDetailCell else { return UITableViewCell() }
                cell.contentImage.image = UIImage(data: image)
                return cell
            case 1:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: ContentDetailCell.identifier, for: indexPath) as? ContentDetailCell else { return UITableViewCell() }
                cell.contentTitle.text = "Question"
                cell.contentText.text = material.question
                cell.contentBackground.backgroundColor = UIColor(named: "darkBlue")
                return cell
            case 2:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: ContentDetailCell.identifier, for: indexPath) as? ContentDetailCell else { return UITableViewCell() }
                cell.contentTitle.text = "Answer"
                cell.contentText.text = material.answer
                
                if let color = material.color {
                    cell.contentBackground.backgroundColor = UIColor(named: color)
                }
                
                return cell
            default:
                return UITableViewCell()
            }
        } else {
            switch indexPath.row {
            case 0:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: ContentDetailCell.identifier, for: indexPath) as? ContentDetailCell,
                      let material = material else { return UITableViewCell() }
                cell.contentTitle.text = "Question"
                cell.contentText.text = material.question
                cell.contentBackground.backgroundColor = UIColor(named: "darkBlue")
                return cell
            case 1:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: ContentDetailCell.identifier, for: indexPath) as? ContentDetailCell,
                      let material = material else { return UITableViewCell() }
                cell.contentTitle.text = "Answer"
                cell.contentText.text = material.answer
                
                if let color = material.color {
                    cell.contentBackground.backgroundColor = UIColor(named: color)
                }
                return cell
            default:
                return UITableViewCell()
            }
        }
    }
}

// MARK: - Protocol Delegate
extension DetailMaterialController: UpdateDetailMaterial {
    func updateMaterial(with material: Material) {
        self.material = material
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
