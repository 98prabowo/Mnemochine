//
//  ContentDetailCell.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 24/08/21.
//

import UIKit

class ContentDetailCell: UITableViewCell, NibGenerator {
    @IBOutlet weak var contentBackground: UIView!
    @IBOutlet weak var contentTitle: UILabel!
    @IBOutlet weak var contentText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentBackground.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
