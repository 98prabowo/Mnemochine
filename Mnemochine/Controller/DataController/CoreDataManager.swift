//
//  CoreDataManager.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 24/08/21.
//

import Foundation
import UIKit
import CoreData

class CoreDataManager {
    static let shared: CoreDataManager = CoreDataManager()
    
    /// CoreData context
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    /// Save item to CoreData.
    func saveData() {
        do {
            try context.save()
        } catch {
            print("Error save CoreData: \(error.localizedDescription)")
        }
    }
    
    /// Fetch item to CoreData.
    ///
    /// - Returns: Array of `TempData` Object.
    func fetchData(with predicate: String? = nil) -> [Subject] {
        var data = [Subject]()
        if let pred = predicate {
            do {
                let request: NSFetchRequest<Subject> = Subject.fetchRequest()
                let predicate = NSPredicate(format: pred)
                request.predicate = predicate
                request.returnsObjectsAsFaults = false
                data = try context.fetch(request)
            } catch {
                print("Error fetch CoreData: \(error.localizedDescription)")
            }
        } else {
            do {
                let request: NSFetchRequest<Subject> = Subject.fetchRequest()
                request.returnsObjectsAsFaults = false
                data = try context.fetch(request)
            } catch {
                print("Error fetch CoreData: \(error.localizedDescription)")
            }
        }
        
        return data
    }
    
    /// Delete item from CoreData.
    ///
    /// - Parameters:
    ///     - object: An instance of NSManagedObject that will be deleted.
    func deleteData(_ object: Subject) {
        context.delete(object)
        CoreDataManager.shared.saveData()
    }
}
