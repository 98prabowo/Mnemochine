//
//  WatchManager.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 24/08/21.
//

import Foundation
import UIKit
import WatchConnectivity

class WatchManager: NSObject {
    static let shared: WatchManager = WatchManager()
    
    private var watchSession: WCSession?
    
    override init() {
        super.init()
        
        if WCSession.isSupported() {
            self.watchSession = WCSession.default
            self.watchSession?.delegate = self
            self.watchSession?.activate()
        }
    }
    
    func createData(subject: Subject) -> Data? {
        var subjectResult: SubjectData?
        if let name = subject.name,
           let materials = subject.materialChild?.allObjects as? [Material] {
            
            var materialsResult = [MaterialData]()
            for material in materials {
                if let question = material.question,
                   let answer = material.answer,
                   let color = material.color {
                    let materialData = MaterialData(imageString: material.imageString,
                                                    image: material.image,
                                                    question: question,
                                                    answer: answer,
                                                    color: color,
                                                    result: material.state,
                                                    isFromUser: material.isFromUser)
                    materialsResult.append(materialData)
                }
                
            }
            
            let subjectData = SubjectData(name: name,
                                          materials: materialsResult)
            subjectResult = subjectData
        }
        
        guard let result = subjectResult else { return nil }
        return encodeData(subject: result)
    }
    
    func encodeData(subject: SubjectData) -> Data? {
        var data: Data?
        do {
            data = try JSONEncoder().encode(subject)
        } catch {
            print("Error encode: \(error.localizedDescription)")
        }
        return data
    }
    
    /// Transfer dictionary data to WatchOS.
    ///
    /// - Parameters:
    ///     - data: A dictionary of `String` to `Any` that will be transfered to WatchOS.
    func sendToWatch(this data: [String: Any]) {
        do {
            try watchSession?.updateApplicationContext(data)
        } catch {
            print("Error sending dictionary \(data) to Apple Watch!")
            print("Error: \(error.localizedDescription)")
        }
    }
    
    func sendMessage(this data: [String: Any]) {
        if let session = watchSession, session.isReachable {
            session.sendMessage(data, replyHandler: nil, errorHandler: nil)
        }
    }
}

extension WatchManager: WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        if let session = self.watchSession {
            print("isPaired: \(session.isPaired), isWatchAppInstalled: \(session.isWatchAppInstalled)")
        }
        print("Activation did complete")
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        print("Session did become inactive")
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        print("Session did deactivate")
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("Did receive message")
    }
}
