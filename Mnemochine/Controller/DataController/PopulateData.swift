//
//  PopulateData.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 24/08/21.
//

import Foundation
import UIKit

class PopulateData {
    static let shared: PopulateData = PopulateData()
    
    private var context = CoreDataManager.shared.context
    private var materialData = [MaterialData]()
    private let symbolTitle = ["Communication", "Weather", "Objects & Tools", "Devices", "Connectivity", "Transportation", "Human", "Nature", "Editing", "Text Formatting", "Media", "Keyboard", "Commerce", "Time", "Health", "Shapes", "Arrows", "Indices", "Math"]
    
    /// Start to populate CoreData.
    func start() {
        for title in symbolTitle {
            var colors = ["red", "green", "blue", "yellow", "orange", "pink"]
            colors.shuffle()
            
            if let data = GetSFSymbol().readLocalFile(),
               let color = colors.first {
                createArray(of: title, with: data, and: color)
            }
            
            createSubjects(name: "\(title) Symbols", with: materialData)
            self.materialData.removeAll()
        }
        
    }
    
    /// Append materialData item.
    ///
    /// - Parameters:
    ///     - title: SF Symbol category title.
    ///     - symbols: Array of `SFSymbol` data.
    ///     - color: String of accent color name.
    private func createArray(of title: String, with symbols: [SFSymbol], and color: String) {
        for data in symbols {
            if data.title == title {
                for image in data.items.prefix(4) {
                    let _material = MaterialData(imageString: image,
                                                 image: UIImage(systemName: image)?.pngData(),
                                                 question: "What symbol is this?",
                                                 answer: "It is called \"\(image)\"",
                                                 color: color,
                                                 result: nil,
                                                 isFromUser: false)
                    self.materialData.append(_material)
                }
            }
        }
    }
    
    /// Create array of subject and save to CoreData.
    ///
    /// - Parameters:
    ///     - name: Topic name.
    ///     - symbols: Array of `MaterialData` data.
    private func createSubjects(name: String, with materialsData: [MaterialData]) {
        let materials = createMaterial(with: materialsData)
        let subjectData = Subject(context: context)
        subjectData.name = name
        subjectData.addToMaterialChild(NSSet(array: materials))
        CoreDataManager.shared.saveData()
    }
    
    /// Create array of instance `Material` from array of instance `MaterialData`.
    ///
    /// - Parameters:
    ///     - materialsData: Array of `MaterialData` Object.
    /// - Returns: Array of `Material` Object.
    private func createMaterial(with materialsData: [MaterialData]) -> [Material] {
        var result = [Material]()
        for material in materialsData {
            let materialData = Material(context: context)
            if let imageName = material.imageString,
               let image = UIImage(systemName: imageName)?.pngData() {
                materialData.image = image
                materialData.imageString = imageName
            }
            materialData.question = material.question
            materialData.answer = material.answer
            materialData.isFromUser = material.isFromUser
            materialData.color = material.color
            materialData.id = UUID()
            result.append(materialData)
        }
        return result
    }
}
