//
//  NewMaterialQuestionController.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 24/08/21.
//

import UIKit

class NewMaterialQuestionController: UIViewController, NibGenerator {
    @IBOutlet weak var backgroundQuestion: UIView!
    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var pageSubtitle: UILabel!
    @IBOutlet weak var questionView: UITextView!
    @IBOutlet weak var navigationButton: UIButton!
    
    let context = CoreDataManager.shared.context
    private var subject: Subject?
    private var material: Material?
    var newMaterialImageController: NewMaterialImageController!
    var startContentController: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationButton.setTitle("Add Question", for: .normal)
        self.navigationButton.layer.cornerRadius = 10
        self.navigationButton.backgroundColor = UIColor(named: LocalColors.darkBlue.rawValue)
    }
    
    func configure(with material: Material, and subject: Subject) {
        self.subject = subject
        self.material = material
    }
    
    @IBAction func navigationButtonTapped(_ sender: Any) {
        if let material = self.material,
           let text = questionView.text,
           text != "" {
            material.question = text
            
            let newMaterialColorController = NewMaterialAnswerController(nibName: NewMaterialAnswerController.identifier, bundle: nil)
            newMaterialColorController.newMaterialQuestionController = self
            newMaterialColorController.startColorController = startContentController
            
            if let subject = self.subject,
               let material = self.material {
                newMaterialColorController.configure(with: material, and: subject)
            }
            
            self.present(newMaterialColorController, animated: true, completion: nil)
        } else {
            self.present(PopUpController(title: "Add Your Question", message: "You need to add a question to your material").defaultPopUp(), animated: true, completion: nil)
        }
    }
}
