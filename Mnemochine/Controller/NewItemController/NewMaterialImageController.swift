//
//  NewMaterialImageController.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 24/08/21.
//

import UIKit

class NewMaterialImageController: UIViewController, NibGenerator {
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var navigationButton: UIButton!
    
    private var subject: Subject?
    private var colorPicked: String = ""
    private var hasImage: Bool = false
    let context = CoreDataManager.shared.context
    var newSubjectController: NewSubjectController?
    var startMaterialController: UIViewController!
    
    private lazy var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationButton.setTitle("Next", for: .normal)
        self.navigationButton.layer.cornerRadius = 10
        self.navigationButton.backgroundColor = UIColor(named: LocalColors.darkBlue.rawValue)
        self.imagePicker.delegate = self
    }
    
    func configure(with subject: Subject, and color: String) {
        self.subject = subject
        self.colorPicked = color
    }
    
    func configure(with subject: Subject) {
        self.subject = subject
        
        if let materialArray = subject.materialChild?.allObjects as? [Material],
           let color = materialArray.first?.color {
            self.colorPicked = color
        }
    }
    
    @IBAction func navigationButtonTapped(_ sender: Any) {
        let material = Material(context: context)
        material.color = self.colorPicked
        
        if let imageData = self.imageButton.backgroundImage(for: .normal)?.pngData(),
           self.hasImage {
            material.image = imageData
        } else {
            material.image = nil
        }
        
        let newMaterialContentController = NewMaterialQuestionController(nibName: NewMaterialQuestionController.identifier, bundle: nil)
        newMaterialContentController.newMaterialImageController = self
        newMaterialContentController.startContentController = startMaterialController
        if let subject = self.subject {
            newMaterialContentController.configure(with: material, and: subject)
        }
        
        self.imageButton.setBackgroundImage(UIImage(named: "addImage"), for: .normal)
        self.present(newMaterialContentController, animated: true, completion: nil)
    }
    
    @IBAction func imageTapped(_ sender: Any) {
        self.imagePicker.sourceType = .photoLibrary
        self.imagePicker.allowsEditing = true
        self.navigationButton.setTitle("Add Image", for: .normal)
        self.present(imagePicker, animated: true, completion: nil)
    }
}

// MARK: - Image Picker Delegate
extension NewMaterialImageController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let imageData = info[.editedImage] as? UIImage {
            self.imageButton.setBackgroundImage(imageData, for: .normal)
            self.hasImage = true
        }
        
        self.dismiss(animated: true, completion: nil)
    }
}
