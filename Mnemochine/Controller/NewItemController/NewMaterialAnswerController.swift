//
//  NewMaterialAnswerController.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 24/08/21.
//

import UIKit

class NewMaterialAnswerController: UIViewController, NibGenerator {
    @IBOutlet weak var answerBackground: UIView!
    @IBOutlet weak var answerView: UITextView!
    @IBOutlet weak var createAgainButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    
    private var subject: Subject?
    private var material: Material?
    var newMaterialQuestionController: NewMaterialQuestionController!
    var startColorController: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.createAgainButton.setTitle("Create Material Again", for: .normal)
        self.createAgainButton.layer.cornerRadius = 10
        self.createAgainButton.backgroundColor = UIColor(named: LocalColors.darkBlue.rawValue)
        self.doneButton.setTitle("Done", for: .normal)
        self.doneButton.layer.cornerRadius = 10
        self.doneButton.backgroundColor = UIColor(named: LocalColors.darkBlue.rawValue)
    }
    
    func configure(with material: Material, and subject: Subject) {
        self.subject = subject
        self.material = material
    }
    
    @IBAction func createAgainTapped(_ sender: Any) {
        if let material = self.material,
           let text = answerView.text,
           text != "" {
            material.answer = text
            
            if let subject = self.subject,
               let material = self.material {
                subject.addToMaterialChild(material)
                CoreDataManager.shared.saveData()
            }
            
            let newMaterialImageController = NewMaterialImageController(nibName: NewMaterialImageController.identifier, bundle: nil)
            
            if let subject = self.subject,
               let color = self.material?.color {
                newMaterialImageController.configure(with: subject, and: color)
            }
            
            newMaterialQuestionController.newMaterialImageController.dismiss(animated: true, completion: nil)
        } else {
            self.present(PopUpController(title: "Add Your Answer", message: "You need to add an answer before creating another material").defaultPopUp(), animated: true, completion: nil)
        }
    }
    
    @IBAction func doneTapped(_ sender: Any) {
        if let material = self.material,
           let text = answerView.text,
           text != "" {
            material.answer = text
            
            if let subject = self.subject {
                subject.addToMaterialChild(material)
                CoreDataManager.shared.saveData()
            }
            
            if let subjectName = self.subject?.name {
                // Post notification center
                let notificationData: [String: String] = ["name": subjectName]
                let name = Notification.Name(refreshHomeKey)
                NotificationCenter.default.post(name: name, object: nil, userInfo: notificationData)
            }
            
            // Get back to root caller
            if let newSubject = newMaterialQuestionController.newMaterialImageController.newSubjectController {
                newSubject.startSubjectController.dismiss(animated: true, completion: nil)
            } else {
                newMaterialQuestionController.newMaterialImageController.startMaterialController.dismiss(animated: true, completion: nil)
            }
        } else {
            self.present(PopUpController(title: "Add Your Answer", message: "You need to add an answer, before finish adding material").defaultPopUp(), animated: true, completion: nil)
        }
    }
}
