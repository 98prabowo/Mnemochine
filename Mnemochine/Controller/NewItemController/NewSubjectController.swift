//
//  NewSubjectController.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 24/08/21.
//

import UIKit

class NewSubjectController: UIViewController, NibGenerator {
    @IBOutlet weak var navigationButton: UIButton!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet var colorButtons: [UIButton]!
    
    let context = CoreDataManager.shared.context
    private var colorPicked: String = ""
    var startSubjectController: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let colors: [LocalColors] = [.red, .blue, .yellow, .green, .pink, .orange]
        for (index, button) in colorButtons.enumerated() {
            button.backgroundColor = UIColor(named: colors[index].rawValue)
            button.layer.cornerRadius = 10
            button.tag = index
        }
        
        self.navigationButton.setTitle("Next", for: .normal)
        self.navigationButton.layer.cornerRadius = 10
        self.navigationButton.backgroundColor = UIColor(named: LocalColors.darkBlue.rawValue)
    }
    
    private func clearBorder(except selectedButton: UIButton) {
        for button in self.colorButtons {
            if button != selectedButton {
                button.layer.borderWidth = 0.0
            }
        }
    }
    
    @IBAction func colorButtonTapped(_ sender: UIButton) {
        sender.layer.borderWidth = 5.0
        sender.layer.borderColor = UIColor.label.cgColor
        clearBorder(except: sender)
        
        switch sender.tag {
        case 0:
            self.colorPicked = LocalColors.red.rawValue
        case 1:
            self.colorPicked = LocalColors.blue.rawValue
        case 2:
            self.colorPicked = LocalColors.yellow.rawValue
        case 3:
            self.colorPicked = LocalColors.green.rawValue
        case 4:
            self.colorPicked = LocalColors.pink.rawValue
        case 5:
            self.colorPicked = LocalColors.orange.rawValue
        default:
            self.colorPicked = LocalColors.red.rawValue
        }
    }
    
    @IBAction func navigationButtonTapped(_ sender: Any) {
        if let name = textField.text,
           name != "",
           colorPicked != "" {
            let newMaterialImageController = NewMaterialImageController(nibName: NewMaterialImageController.identifier, bundle: nil)
            newMaterialImageController.newSubjectController = self
            newMaterialImageController.startMaterialController = startSubjectController
            
            let subject = Subject(context: self.context)
            subject.name = name
            newMaterialImageController.configure(with: subject, and: self.colorPicked)
            self.present(newMaterialImageController, animated: true, completion: nil)
        } else {
            self.present(PopUpController(title: "Fill Your Data", message: "You need to add topic name and pick topic color").defaultPopUp(), animated: true, completion: nil)
        }
    }
}
