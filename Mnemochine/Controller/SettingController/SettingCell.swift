//
//  SettingCell.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 26/08/21.
//

import UIKit

class SettingCell: UITableViewCell, NibGenerator {
    @IBOutlet weak var backgroundTopic: UIView!
    @IBOutlet weak var materialCount: UILabel!
    @IBOutlet weak var topicTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundTopic.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
