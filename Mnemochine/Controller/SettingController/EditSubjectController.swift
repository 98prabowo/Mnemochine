//
//  EditSubjectController.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 27/08/21.
//

import UIKit

class EditSubjectController: UIViewController, NibGenerator {
    @IBOutlet weak var navigationButton: UIButton!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet var colorButtons: [UIButton]!
    
    private var subject: Subject?
    private var colorPicked: String = ""
    var updateSubject: UpdateItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let colors: [LocalColors] = [.red, .blue, .yellow, .green, .pink, .orange]
        for (index, button) in colorButtons.enumerated() {
            button.backgroundColor = UIColor(named: colors[index].rawValue)
            button.layer.cornerRadius = 10
            button.tag = index
            
            if let subject = self.subject,
               let materials = subject.materialChild?.allObjects as? [Material],
               let color = materials.first?.color,
               colors[index].rawValue == color {
                button.layer.borderWidth = 5.0
                button.layer.borderColor = UIColor.label.cgColor
                clearBorder(except: button)
            }
        }
        
        if let name = subject?.name {
            self.textField.text = name
        }
        
        self.navigationButton.setTitle("Save", for: .normal)
        self.navigationButton.layer.cornerRadius = 10
        self.navigationButton.backgroundColor = UIColor(named: LocalColors.darkBlue.rawValue)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        updateSubject.refreshItem()
    }
    
    func configure(with subject: Subject) {
        self.subject = subject
    }
    
    private func clearBorder(except selectedButton: UIButton) {
        for button in self.colorButtons {
            if button != selectedButton {
                button.layer.borderWidth = 0.0
            }
        }
    }
    
    @IBAction func colorButtonTapped(_ sender: UIButton) {
        sender.layer.borderWidth = 5.0
        sender.layer.borderColor = UIColor.label.cgColor
        clearBorder(except: sender)
        
        switch sender.tag {
        case 0:
            self.colorPicked = LocalColors.red.rawValue
        case 1:
            self.colorPicked = LocalColors.blue.rawValue
        case 2:
            self.colorPicked = LocalColors.yellow.rawValue
        case 3:
            self.colorPicked = LocalColors.green.rawValue
        case 4:
            self.colorPicked = LocalColors.pink.rawValue
        case 5:
            self.colorPicked = LocalColors.orange.rawValue
        default:
            self.colorPicked = LocalColors.red.rawValue
        }
    }
    
    @IBAction func navigationButtonTapped(_ sender: Any) {
        if let name = textField.text,
           name != "",
           colorPicked != "" {
            if let subject = self.subject,
               let materials = subject.materialChild,
               let materialsArray = materials.allObjects as? [Material] {
                subject.name = name
                subject.removeFromMaterialChild(materials)
                
                for material in materialsArray {
                    material.color = self.colorPicked
                    subject.addToMaterialChild(material)
                }
                
                CoreDataManager.shared.saveData()
                self.dismiss(animated: true, completion: nil)
            } else {
                self.present(PopUpController(title: "Something Wrongs", message: "There are issue with the data that you want to save").defaultPopUp(), animated: true, completion: nil)
            }
        } else {
            self.present(PopUpController(title: "Fill Your Data", message: "You need to fill topic name and pick topic color").defaultPopUp(), animated: true, completion: nil)
        }
    }
}

