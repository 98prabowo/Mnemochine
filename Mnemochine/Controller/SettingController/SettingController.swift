//
//  SettingController.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 26/08/21.
//

import UIKit

class SettingController: UIViewController, NibGenerator {
    @IBOutlet weak var tableView: UITableView!
    
    private var subjects = [Subject]()
    var updateSubjects: UpdateItem!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Delete Subject"
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.subjects = CoreDataManager.shared.fetchData()
        
        self.title = "Delete Subject"
        self.navigationController?.navigationBar.prefersLargeTitles = true

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(SettingCell.nib(), forCellReuseIdentifier: SettingCell.identifier)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        updateSubjects.refreshItem()
    }
}

// MARK: - Table View
extension SettingController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SettingCell.identifier, for: indexPath) as? SettingCell else { return UITableViewCell() }
        cell.selectionStyle = .none
        
        if let materialCount = self.subjects[indexPath.row].materialChild?.count {
            cell.materialCount.text = "\(materialCount) Materials"
        } else {
            cell.materialCount.text = "No Materials"
        }
        
        
        if let materials = subjects[indexPath.row].materialChild?.allObjects as? [Material],
           let color = materials.first?.color {
            cell.backgroundTopic.backgroundColor = .clear
            cell.backgroundTopic.layer.borderWidth = 5.0
            cell.backgroundTopic.layer.borderColor = UIColor(named: color)?.cgColor
        }
        
        cell.topicTitle.text = self.subjects[indexPath.row].name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        let editSubjectController = EditSubjectController(nibName: EditSubjectController.identifier, bundle: nil)
        editSubjectController.configure(with: subjects[indexPath.row])
        editSubjectController.updateSubject = self
        self.present(editSubjectController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            CoreDataManager.shared.deleteData(self.subjects[indexPath.row])
            self.subjects.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
        } 
    }
}

// MARK: - Protocol Delegate
extension SettingController: UpdateItem {
    func refreshItem() {
        self.subjects = CoreDataManager.shared.fetchData()
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
