//
//  DetailListController.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 24/08/21.
//

import UIKit

class DetailListController: UIViewController, NibGenerator {
    @IBOutlet weak var tableView: UITableView!
    
    private var subject: Subject?
    private var materials = [Material]()
    var deleteMaterial: UpdateItem!
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let subject = self.subject {
            self.title = subject.name
        } else {
            self.title = "Materials"
        }
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationController?.navigationBar.tintColor = .label
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.registerTableCell()
        
        createObserver()
        self.navigationItem.rightBarButtonItems = createRightButtons()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        deleteMaterial.refreshItem()
    }
    
    @objc func sendTapped(_ sender: Any) {
        if let subject = self.subject {
            if let data = WatchManager.shared.createData(subject: subject) {
                WatchManager.shared.sendMessage(this: ["subjects": data, "forceUpdate": UUID().uuidString])
            }
        }
    }
    
    @objc func addTapped(_ sender: Any) {
        if let subject = self.subject {
            let newMaterialController = NewMaterialImageController(nibName: NewMaterialImageController.identifier, bundle: nil)
            newMaterialController.startMaterialController = self
            newMaterialController.configure(with: subject)
            self.navigationController?.present(newMaterialController, animated: true, completion: nil)
        }
    }
    
    private func createRightButtons() -> [UIBarButtonItem] {
        let sendButton = UIBarButtonItem(image: UIImage(systemName: "square.and.arrow.up"), style: .plain, target: self, action: #selector(sendTapped))
        let addButton = UIBarButtonItem(image: UIImage(systemName: "square.and.pencil"), style: .plain, target: self, action: #selector(addTapped))
        return [sendButton, addButton]
    }
    
    private func registerTableCell() {
        self.tableView.register(DetailListCell.nib(), forCellReuseIdentifier: DetailListCell.identifier)
    }
    
    func configure(with subject: Subject) {
        self.subject = subject
        if let materialArray = subject.materialChild?.allObjects as? [Material] {
            self.materials = materialArray
        }
    }
}

// MARK: - Table View
extension DetailListController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return materials.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DetailListCell.identifier, for: indexPath) as? DetailListCell else { return UITableViewCell() }
        if !materials[indexPath.row].isFromUser,
           let imageName = materials[indexPath.row].imageString {
            cell.photo.image = UIImage(systemName: imageName)
        } else {
            if let data = materials[indexPath.row].image {
                cell.photo.image = UIImage(data: data)
            }
        }
        
        if let question = materials[indexPath.row].question {
            cell.materialTitle.text = question
            cell.materialTitle.textColor = UIColor(named: "blueText")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        if let subject = self.subject {
            let detailMaterialController = DetailMaterialController(nibName: DetailMaterialController.identifier, bundle: nil)
            detailMaterialController.refreshMaterialList = self
            detailMaterialController.configure(with: subject, and: self.materials[indexPath.row])
            self.navigationController?.pushViewController(detailMaterialController, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if let subject = self.subject {
                subject.removeFromMaterialChild(self.materials[indexPath.row])
                CoreDataManager.shared.saveData()
                self.materials.remove(at: indexPath.row)
                self.tableView.deleteRows(at: [indexPath], with: .automatic)
            }
        }
    }
}

// MARK: - Protocol Delegate
extension DetailListController: UpdateItem {
    func refreshItem() {
        if let subject = self.subject,
           let subjectName = subject.name {
            let subjectFull = CoreDataManager.shared.fetchData(with: "name CONTAINS '\(subjectName)'")
            
            if let data = subjectFull.first {
                self.subject = data
            }
            
            if let materialArray = subject.materialChild?.allObjects as? [Material] {
                self.materials = materialArray
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
}

// MARK: - Observer Notification
extension DetailListController {
    func createObserver() {
        let name = Notification.Name(refreshHomeKey)
        NotificationCenter.default.addObserver(self, selector: #selector(addItem(_:)), name: name, object: nil)
    }
    
    @objc private func addItem(_ notification: NSNotification) {
        if let dict = notification.userInfo as NSDictionary?,
           let subjectName = dict["name"] as? String {
            
            let subjectFull = CoreDataManager.shared.fetchData(with: "name CONTAINS '\(subjectName)'")
            
            if let data = subjectFull.first {
                self.subject = data
            }
            
            if let subject = self.subject,
               let materialArray = subject.materialChild?.allObjects as? [Material] {
                self.materials = materialArray
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
}
