//
//  DetailListCell.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 24/08/21.
//

import UIKit

class DetailListCell: UITableViewCell, NibGenerator {
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var materialTitle: UILabel!
    @IBOutlet weak var imageBG: UIView!
    @IBOutlet weak var photo: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.background.layer.cornerRadius = 10
        self.imageBG.backgroundColor = UIColor(named: "darkBlue")
        self.imageBG.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
