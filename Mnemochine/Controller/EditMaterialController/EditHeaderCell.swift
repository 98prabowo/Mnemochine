//
//  EditHeaderCell.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 27/08/21.
//

import UIKit

class EditHeaderCell: UITableViewCell, NibGenerator {
    @IBOutlet weak var header: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
