//
//  EditMaterialController.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 27/08/21.
//

import UIKit

class EditMaterialController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    private var subject: Subject?
    var material: Material?
    var detailMaterialController: DetailMaterialController!
    
    lazy var imagePicker = UIImagePickerController()
    
    static let identifier = "EditMaterialController"
    
    static func nib() -> UINib {
        return UINib(nibName: "EditMaterialController", bundle: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Edit Material"
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Edit Material"
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.registerTableCell()
        self.imagePicker.delegate = self
    }
    
    private func registerTableCell() {
        self.tableView.register(EditHeaderCell.nib(), forCellReuseIdentifier: EditHeaderCell.identifier)
        self.tableView.register(EditImageCell.nib(), forCellReuseIdentifier: EditImageCell.identifier)
        self.tableView.register(EditContentCell.nib(), forCellReuseIdentifier: EditContentCell.identifier)
        self.tableView.register(EditSaveCell.nib(), forCellReuseIdentifier: EditSaveCell.identifier)
    }
    
    func configure(with subject: Subject, and material: Material) {
        self.subject = subject
        self.material = material
    }
    
    func refreshDataFromCell(with material: Material) {
        self.material = material
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

// MARK: - Table View
extension EditMaterialController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: EditHeaderCell.identifier, for: indexPath) as? EditHeaderCell else { return UITableViewCell() }
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: EditImageCell.identifier, for: indexPath) as? EditImageCell,
                  let material = self.material else { return UITableViewCell() }
            cell.selectionStyle = .none
            
            if material.isFromUser,
               let data = material.image {
                cell.editImageButton.setBackgroundImage(UIImage(data: data), for: .normal)
            } else {
                if let imageName = material.imageString {
                    cell.editImageButton.setBackgroundImage(UIImage(systemName: imageName), for: .normal)
                }
            }
            
            cell.buttonImageTappedClosure = { [weak self] in
                self?.imagePicker.sourceType = .photoLibrary
                self?.imagePicker.allowsEditing = true
                self?.present(self!.imagePicker, animated: true, completion: nil)
            }
            
            return cell
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: EditContentCell.identifier, for: indexPath) as? EditContentCell,
                  let material = self.material else { return UITableViewCell() }
            cell.selectionStyle = .none
            cell.editMaterialController = self
            cell.isAnswer = false
            cell.editContentTitle.text = "Question"
            cell.editContentBG.backgroundColor = UIColor(named: LocalColors.darkBlue.rawValue)
            
            if let question = material.question {
                cell.editContentView.text = question
            }
            
            return cell
        case 3:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: EditContentCell.identifier, for: indexPath) as? EditContentCell,
                  let material = self.material else { return UITableViewCell() }
            cell.selectionStyle = .none
            cell.editMaterialController = self
            cell.isAnswer = true
            cell.editContentTitle.text = "Answer"
            
            if let answer = material.answer {
                cell.editContentView.text = answer
            }
            
            if let color = material.color {
                cell.editContentBG.backgroundColor = UIColor(named: color)
            }
            
            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: EditSaveCell.identifier, for: indexPath) as? EditSaveCell else {
                return UITableViewCell() }
            cell.editMaterialController = self
            cell.refreshDetailMaterial = detailMaterialController
            if let subject = self.subject,
               let material = self.material {
                cell.configure(with: subject, and: material)
            }
            cell.selectionStyle = .none
            return cell
        }
    }
}

// MARK: - Image Picker Delegate
extension EditMaterialController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let imageData = info[.editedImage] as? UIImage {
            if let material = self.material {
                material.image = imageData.pngData()
                material.isFromUser = true
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        
        self.dismiss(animated: true, completion: nil)
    }
}
