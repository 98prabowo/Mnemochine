//
//  EditSaveCell.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 27/08/21.
//

import UIKit

class EditSaveCell: UITableViewCell, NibGenerator {
    @IBOutlet weak var saveButton: UIButton!
    
    private var subject: Subject?
    private var material: Material?
    var refreshDetailMaterial: UpdateDetailMaterial!
    var editMaterialController: EditMaterialController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.saveButton.setTitle("Save", for: .normal)
        self.saveButton.layer.cornerRadius = 10
        self.saveButton.backgroundColor = UIColor(named: LocalColors.darkBlue.rawValue)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(with subject: Subject, and material: Material) {
        self.subject = subject
        self.material = material
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        if let subject = self.subject,
           let localMaterial = self.material,
           let materialsArray = subject.materialChild?.allObjects as? [Material] {
            for material in materialsArray {
                if material.id == localMaterial.id {
                    subject.removeFromMaterialChild(material)
                    subject.addToMaterialChild(localMaterial)
                }
            }
        }
        
        CoreDataManager.shared.saveData()
        if let material = self.material {
            refreshDetailMaterial.updateMaterial(with: material)
            editMaterialController.dismiss(animated: true, completion: nil)
        }
    }
}
