//
//  EditContentCell.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 27/08/21.
//

import UIKit

class EditContentCell: UITableViewCell, NibGenerator {
    @IBOutlet weak var editContentTitle: UILabel!
    @IBOutlet weak var editContentBG: UIView!
    @IBOutlet weak var editContentView: UITextView!
    
    var editMaterialController: EditMaterialController!
    var isAnswer: Bool!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.editContentBG.layer.cornerRadius = 10
        self.editContentView.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

// MARK: - Text View
extension EditContentCell: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        if editContentView.text != "",
           let material = editMaterialController.material {
            if !isAnswer {
                material.question = editContentView.text
            } else {
                material.answer = editContentView.text
            }
        }
    }
}
