//
//  EditImageCell.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 27/08/21.
//

import UIKit

class EditImageCell: UITableViewCell, NibGenerator {
    @IBOutlet weak var editImageBG: UIView!
    @IBOutlet weak var editImageBGForeGround: UIView!
    @IBOutlet weak var editImageButton: UIButton!
    
    var buttonImageTappedClosure: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.editImageButton.layer.cornerRadius = 20
        self.editImageBGForeGround.layer.cornerRadius = 20
        self.editImageBGForeGround.backgroundColor = .systemBackground
        self.editImageBG.layer.cornerRadius = 20
        self.editImageBG.backgroundColor = UIColor(named: LocalColors.darkBlue.rawValue)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func editImageTapped(_ sender: Any) {
        self.buttonImageTappedClosure?()
    }
}


