//
//  PopUpController.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 26/08/21.
//

import Foundation
import UIKit

class PopUpController {
    private var title: String
    private var message: String
    
    init(title: String, message: String) {
        self.title = title
        self.message = message
    }
    
    /// Create default popup to present.
    ///
    /// - Returns: Instance of `UIAlertController` Object that can passed to present method.
    func defaultPopUp() -> UIAlertController {
        let alert = UIAlertController(title: self.title, message: self.message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alert
    }
}
