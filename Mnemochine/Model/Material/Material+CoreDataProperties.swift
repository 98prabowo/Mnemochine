//
//  Material+CoreDataProperties.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 27/08/21.
//
//

import Foundation
import CoreData


extension Material {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Material> {
        return NSFetchRequest<Material>(entityName: "Material")
    }

    @NSManaged public var answer: String?
    @NSManaged public var color: String?
    @NSManaged public var image: Data?
    @NSManaged public var imageString: String?
    @NSManaged public var isFromUser: Bool
    @NSManaged public var question: String?
    @NSManaged public var state: String?
    @NSManaged public var id: UUID?
    @NSManaged public var subjectParent: Subject?

}

extension Material : Identifiable {

}
