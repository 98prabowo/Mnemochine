//
//  Subject+CoreDataProperties.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 24/08/21.
//
//

import Foundation
import CoreData


extension Subject {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Subject> {
        return NSFetchRequest<Subject>(entityName: "Subject")
    }

    @NSManaged public var name: String?
    @NSManaged public var materialChild: NSSet?

}

// MARK: Generated accessors for materialChild
extension Subject {

    @objc(addMaterialChildObject:)
    @NSManaged public func addToMaterialChild(_ value: Material)

    @objc(removeMaterialChildObject:)
    @NSManaged public func removeFromMaterialChild(_ value: Material)

    @objc(addMaterialChild:)
    @NSManaged public func addToMaterialChild(_ values: NSSet)

    @objc(removeMaterialChild:)
    @NSManaged public func removeFromMaterialChild(_ values: NSSet)

}

extension Subject : Identifiable {

}
