//
//  SFSymbol.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 24/08/21.
//

import Foundation

struct SFSymbol: Codable {
    let title: String
    let items: [String]
}

class GetSFSymbol {
    var fileName: String = "sf-symbols"
    
    init() {}
    
    init(fileName: String) {
        self.fileName = fileName
    }
    
    /// Read local data.
    ///
    /// - Returns: Array of optional `SFSymbol` Object.
    func readLocalFile() -> [SFSymbol]? {
        do {
            if let bundlePath = Bundle.main.path(forResource: self.fileName, ofType: "json"),
               let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8),
               let data = parse(jsonData: jsonData) {
                return data
            }
        } catch {
            print("read local file error: \(error.localizedDescription)")
        }
        return nil
    }
    
    /// Decode data to specific class/struct.
    ///
    /// - Parameters:
    ///   - jsonData: Data that will decode to `SFSymbol` Instances.
    /// - Returns: Array of optional `SFSymbol` Object.
    private func parse(jsonData: Data) -> [SFSymbol]? {
        do {
            let decodedData = try JSONDecoder().decode([SFSymbol].self, from: jsonData)
            return decodedData
        } catch {
            print("parsing error: \(error.localizedDescription)")
        }
        return nil
    }
}
