//
//  Subject.swift
//  Mnemochine
//
//  Created by Dimas A. Prabowo on 24/08/21.
//

import Foundation

struct SubjectData: Codable {
    let name: String
    let materials: [MaterialData]
}

class MaterialData: Codable {
    let imageString: String?
    let image: Data?
    let question: String
    let answer: String
    var color: String?
    var result: String?
    var isFromUser: Bool
    
    init(imageString: String?, image: Data?, question: String, answer: String, color: String?, result: String?, isFromUser: Bool) {
        self.imageString = imageString
        self.image = image
        self.question = question
        self.answer = answer
        self.color = color
        self.result = result
        self.isFromUser = isFromUser
    }
}

enum Condition: String, Codable {
    case skip
    case strugle
    case normal
    case easy
}

enum LocalColors: String {
    case red
    case blue
    case yellow
    case green
    case orange
    case pink
    case darkBlue
}
